#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<arpa/inet.h>

typedef enum {
	GET,
	POST,
	CONNECT
} method;

typedef struct {
	method method;
	char *host;
	int port;
} request;

static void error(const char *msg);
static request *parse_request(char *req_buff);
static void free_request(request *r);
static int remote_connect(char *host, int port);
// methods
static int connect_loop(int sock, request *r);
static int post_request(int sock, request *r);

void error(const char *msg) {
	printf("%s: %s\n", msg, strerror(errno));
	exit(-1);
}

request *parse_request(char *req_buff) {
	request *req = malloc(sizeof(request));
	req->method = GET;
	req->host = NULL;
	req->port = -1;

	char * fl = strtok(req_buff, "\n"); // first line
	fl = strtok(fl, " "); // parse method

	if(strcmp("CONNECT", fl) == 0) req->method = CONNECT;
	if(strcmp("GET"    , fl) == 0) req->method = GET;
	if(strcmp("POST"   , fl) == 0) req->method = POST;

	switch(req->method) {
		case GET:
			break;
		case POST:
			break;
		case CONNECT:
			fl = strtok(NULL, ":"); // host
			req->host = malloc(sizeof(char)*strlen(fl)); // allocate enough space
			strcpy(req->host, fl); // cpy to the allocated space

			fl = strtok(NULL, " "); // parse port as in host:port
			req->port = (atoi(fl) != 443) ? 80 : 443;
			break;
	}

	return req;
}

int remote_connect(char *host, int port) {
	struct hostent *server;
	struct sockaddr_in saddr;
	int s;

	s = socket(AF_INET, SOCK_STREAM, 0);

	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);

	server = gethostbyname(host);
	bcopy((char*)server->h_addr, (char*) &saddr.sin_addr.s_addr, server->h_length);

	if(connect(s, (struct sockaddr *) &saddr, sizeof(saddr)) < 0)
		error("connect() in remote_connect");

	return s;
}

void free_request(request *r) {
	free(r->host);
	free(r);
}

int connect_loop(int sock, request *r) {
	const char *ok = "HTTP/1.1 200 OK\r\n\r\n";
	size_t len = strlen(ok);
	if(send(sock, ok, len, 0) != len)
		return -1;

	ssize_t n;
	int s;
	char b[999999] = {'\0'};
	s = remote_connect(r->host, r->port);
	while(1) {
		n = recv(sock, b, sizeof(b), 0);
		printf("%d: recebi %ld do firefox\n", sock, n);
		if(send(s, b, n, 0) != n) {
			puts("different amount of bytes to forward");
		}
		printf("%d: enviei %ld pro servidor\n", s, n);
		memset(b, '\0', sizeof(b));
		n = recv(s, b, sizeof(b), 0);
		printf("%d: recebi %ld do servidor\n", s, n);
		n = send(sock, b, n, 0);
		printf("%d: enviei %ld pro firefox\n", sock, n);
		memset(b, '\0', sizeof(b));

	}
	close(s);
}

int post_request(int sock, request *r) {
	int s;
	s = remote_connect(r->host, r->port);
	close(s);
	return 1;
}

int main(void) {
	struct sockaddr_in saddr, caddr;
	int s, cs, n;
	char buff[256];

	if((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		error("socket() error");
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0)
    	error("setsockopt(SO_REUSEADDR) failed");

	explicit_bzero(&saddr, sizeof(saddr));
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(8008);

	if(bind(s, (struct sockaddr*) &saddr, sizeof(saddr)) < 0)
		error("bind fail");

	listen(s, 5);

	// main loop
	while(1) {
		if((cs = accept(s,
						(struct sockaddr*) &caddr,
						&(socklen_t){sizeof(caddr)} // cool :)
		   )) < 0)
			  error("accept fail");
		if(!fork()) {
			char b[256] = {'\0'};
			n = recv(cs, b, 255, 0);
			request *r = parse_request(b);
			switch(r->method) {
				case GET:
					puts("recebi get e dropei");
					break;
				case POST:
					if(post_request(cs, r) < 0)
						error("post_request");
					break;
				case CONNECT:
					if(connect_loop(cs, r) < 0)
						error("connect_loop");
					break;
			}
			free_request(r);

			memset(b, '\0', 256);
			puts("");
			close(cs);
		}
	}

	return 0;
}
